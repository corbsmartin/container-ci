# container-ci

My hack containerization project.

* Clone source repo with Dockerfile
* Build container image
* Tag and push to container registry

This project uses a custom gitlab-runner.

* Ubuntu 18.04 vm
* Docker 20.10.16
* Git

