#!/usr/bin/env bash
if [ -z "$1" ]
  then
    echo "Provide code repo name, for example: $ ./trigger-pipeline.sh todos-webui"
    exit -1
fi
source ${HOME}/.gitlab/trigger-token.env
CODE_REPOSITORY_NAME=$1
curl -X POST \
     --fail \
     -F token=${TRIGGER_TOKEN_CONTAINER_CI} \
     -F "ref=main" \
     -F "variables[CODE_REPOSITORY_NAME]=${CODE_REPOSITORY_NAME}" \
     https://gitlab.com/api/v4/projects/38061684/trigger/pipeline